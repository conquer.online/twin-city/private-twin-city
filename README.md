# Private Twin City

Twin City is a simple, educational game networking project targeting the [__Conquer Online__](https://co.99.com)  Online game client.

The project is split between three servers: an account server, game server and ai server. The account server authenticates players, while the game server services players in the game world and the ai server handles ai over the NPCs in the world. This simple three-server architecture acts as a good introduction into server programming and concurrency patterns. The server is interoperable with the Conquer Online game client, but a modified client will not be provided.

This still a work in progress and is not recommended to starters. No support will be given to creating events, NPCs or anything like that. But if you want to work with  [Twin City](https://gitlab.com/conquer.online/twin-city) you may report bugs and we will keep the main repository updated with bug fixes to whoever wants to try it.

This project is still in Beta and target the client version 7709 of the Official English client.

The basic client can be downloaded from [here](https://codl.99.com/en_zf/Conquer_v7682.exe).

**We will also work to continuously update the project here, making it compatible with the main source of Conquer Online**

When the live server leaves the Beta Stage, we will start keeping stable versions of Twin City in the [__main__](https://gitlab.com/conquer.online/twin-city/private-twin-city/-/tree/main) main repository, if you download from [__development__](https://gitlab.com/conquer.online/twin-city/private-twin-city/-/tree/development) make sure you know what you are doing and that you are ready to face bugs.

| Patch | Pipeline Status | Quality Gate | Description |
| ----- | --------------- | ------------ | ----------- |
| [__development__](https://gitlab.com/conquer.online/twin-city/private-twin-city/-/tree/develop) | [![pipeline status](https://gitlab.com/conquer.online/twin-city/private-twin-city/badges/develop/pipeline.svg)](https://gitlab.com/conquer.online/twin-city/private-twin-city/-/commits/development) | [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=world-conquer-online_long&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=world-conquer-online_long) | Targets the official 7709 client. |
| [__main__](https://gitlab.com/conquer.online/twin-city/private-twin-city/-/tree/main) | [![pipeline status](https://gitlab.com/conquer.online/twin-city/private-twin-city/badges/main/pipeline.svg)](https://gitlab.com/conquer.online/twin-city/private-twin-city/-/commits/main) | Not published | Targets the official 7709 client. |

Please note: This is a skeleton / base project, meaning no game features have been implemented beyond login.

## Getting Started

Before setting up the project, download and install the following:

* [.NET 8](https://dotnet.microsoft.com/download) - Primary language compiler
* [MariaDB](https://mariadb.org/) - Recommended flavor of MySQL for project databases 
* [MySQL Workbench](https://dev.mysql.com/downloads/workbench/) - Recommended SQL editor and database importer
* [Visual Studio](https:/.visualstudio.com/) - Recommended editor for modifying and building project files

In a terminal, run the following commands to build the project (or build with Shift+Ctrl+B):

```
dotnet restore
dotnet build
```

This project has been tested already with Debian and will work well in other environments.

## Login Config
